# Zemoga Test

Hello, this is my take on the iOS technical test for Zemoga.

## Installation

You can clone this BitBucket repository and by double click the zemogaTest.xcodeproj file you can open the project and run it into a simulator or with iOS 15 or higher.


## Explanation
So I use a Model-View-ViewModel-Presenter architecture mixed with a Combine pattern, since I am using SwiftUI as a framework to build the UI, it makes easier to control the states of the app by making the ViewModel and @ObservableObject the UI can response in real time on what is happening with the services on the back

appreciate your time in order to check out this test.

## Thank You so Much!!