//
//  zemogaTestApp.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 11/12/21.
//

import SwiftUI

@main
struct zemogaTestApp: App {
    var viewModel: MainContentViewModel = MainContentViewModel()
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: viewModel,
                        presenter: MainPresenter(viewModel: viewModel))
        }
    }
}
