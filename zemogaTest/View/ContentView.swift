//
//  ContentView.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 11/12/21.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var viewModel: MainContentViewModel
    var presenter: MainPresenter
    
    @State var allSelected: Bool = true
    @State var favsSelected: Bool = false
    
    var body: some View {
        
        NavigationView{
            
            VStack {
                
                HStack(alignment: .center) {
                    Spacer()
                        .frame(width: 20)
                        .padding()

                    Text("Posts")
                        .font(.headline)
                        .frame(maxWidth: .infinity, maxHeight: 40)

                    Image(systemName: "arrow.clockwise")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .padding()
                        .onTapGesture {
                            self.presenter.onViewAppear()
                        }

                }.background(.green)
                
                HStack {
                    Text("All")
                        .frame(maxWidth: .infinity, maxHeight: 40)
                        .font(.headline)
                        .foregroundColor(allSelected ? .white : .green)
                        .background(allSelected ? .green : .white)
                        .onTapGesture {
                            self.allSelected = true
                            self.favsSelected = false
                        }
                    
                    Text("Favorites")
                        .frame(maxWidth: .infinity, maxHeight: 40)
                        .font(.headline)
                        .foregroundColor(favsSelected ? .white : .green)
                        .background(favsSelected ? .green : .white)
                        .onTapGesture {
                            self.allSelected = false
                            self.favsSelected = true
                        }
                }
                .border(Color.green, width: 2)
                .padding(.leading, 30)
                .padding(.trailing, 30)
                
                ScrollView(.vertical) {
                    LazyVStack(alignment: .leading, spacing: 20) {
                        if !(self.viewModel.posts?.count == 0){
                            if self.viewModel.posts != nil {
                                ForEach(0..<self.presenter.postList.count, id: \.self) { index in
                                    
                                    if favsSelected {
                                        if self.presenter.postList[index].isFavorite {
                                            NavigationLink(
                                                destination: PostDetailView(
                                                    viewModel: self.viewModel,
                                                    presenter: self.presenter,
                                                    post:self.presenter.postList[index]),
                                                label: {
                                                    PostCellView(selected: self.presenter.postList[index].isViewed,
                                                                 title: self.presenter.postList[index].title,
                                                                 isFavorite: favsSelected)
                                                })
                                            Divider()
                                        }
                                    } else {
                                        NavigationLink(
                                            destination: PostDetailView(
                                                viewModel: self.viewModel,
                                                presenter: self.presenter,
                                                post: self.presenter.postList[index]),
                                            label: {
                                                PostCellView(selected: self.presenter.postList[index].isViewed,
                                                             title: self.presenter.postList[index].title,
                                                             isFavorite: favsSelected)
                                            })
                                        Divider()
                                    }
                                    
                                }
                            }
                        }
                    }.padding()
                }.onAppear{
                    if presenter.shouldReload {
                        presenter.onViewAppear()
                        presenter.shouldReload = false
                    }
                }
                
                Button {
                    self.presenter.updateData(postsUpdated: [])
                } label: {
                    Text("Delete All")
                        .frame(maxWidth: .infinity, maxHeight: 40)
                        .font(.headline)
                        .foregroundColor(.white)
                        .background(.red)
                        .padding()
                }
            }.navigationBarHidden(true)
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: MainContentViewModel(),
                    presenter: MainPresenter(viewModel: MainContentViewModel()))
    }
}
