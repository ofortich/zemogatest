//
//  MainContentViewModel.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 11/12/21.
//

import Foundation

class MainContentViewModel: ObservableObject{
    
    @Published var isLoading: Bool = false
    @Published var posts: [Post]?
    @Published var userSelected: User = User()
    @Published var comments: [Comment] = []
    
}
