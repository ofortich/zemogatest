//
//  PostCellView.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 11/12/21.
//

import SwiftUI

struct PostCellView: View {
    
    var selected: Bool
    var title: String
    var isFavorite: Bool
    
    var body: some View {
        HStack {
            
            Image(systemName: isFavorite ? "star.fill" : "circlebadge.fill")
                .resizable()
                .frame(width: 10, height: 10)
                .foregroundColor(isFavorite ? .yellow : selected ? .white : .blue)
                .onTapGesture {
                    print()
                }
            
            Text(title)
                .fontWeight(.semibold)
                .lineLimit(3)
                .minimumScaleFactor(0.5)
                .foregroundColor(.black)
            
            Spacer()
            
            Image(systemName: "chevron.right")
                .resizable()
                .frame(width: 8, height: 12)
                .foregroundColor(.gray)
            
        }
    }
}

struct PostCellView_Previews: PreviewProvider {
    static var previews: some View {
        PostCellView(selected: true,
                     title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                     isFavorite: false)
    }
}
