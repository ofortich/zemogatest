//
//  PostDetailView.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 12/12/21.
//

import SwiftUI

struct PostDetailView: View {
    @ObservedObject var viewModel: MainContentViewModel
    var presenter: MainPresenter
    var post: Post
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 10) {
            
            VStack(alignment: .leading, spacing: 10){
                Text("Description").font(.headline)
                Text(post.body)
                    .font(.body)
            }.padding()
            
            VStack(alignment: .leading, spacing: 10) {
                Text("User").font(.headline)
                
                Text("Name: \(self.viewModel.userSelected.name)")
                Text("Email: \(self.viewModel.userSelected.email)")
                Text("Phone: \(self.viewModel.userSelected.phone)")
                Text("Website: \(self.viewModel.userSelected.website)")
            }.padding()
            
            Text("COMMENTS")
                .font(.headline)
                .frame(maxWidth: .infinity,
                       maxHeight: 30,
                       alignment: .leading)
                .background(.gray)
            
            List {
                ForEach(self.viewModel.comments) { comment in
                    Text(comment.body)
                }
            }
            
        }
        .onAppear(perform: {
            self.presenter.onDetailViewAppear(userID: post.userID)
            self.presenter.loadComments(postID: self.post.id)
            self.presenter.addToViewed(postID: self.post.id)
        })
        .navigationTitle("Post").toolbar {
            Button{
                self.presenter.addToFavorite(postID: post.id)
            }label: {
                Label("Add To Favorite", systemImage: self.post.isFavorite ? "star.fill" : "star")
                                    .foregroundColor(self.post.isFavorite ? .yellow : .black)
            }
        }
    }
}

struct PostDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PostDetailView(viewModel: MainContentViewModel(), presenter: MainPresenter(viewModel: MainContentViewModel()),
                       post: Post())
    }
}
