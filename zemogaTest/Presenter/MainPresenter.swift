//
//  MainPresenter.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 11/12/21.
//

import Foundation

class MainPresenter {
    
    let postService = PostService()
    let userService = UserService()
    let commentService = CommentService()
    let viewModel: MainContentViewModel
    var postList: [Post] = []
    var userList: [User] = []
    var userSelected: User = User()
    var shouldReload: Bool = true
    
    init (viewModel: MainContentViewModel) {
        self.viewModel = viewModel
    }
    
    func onViewAppear(){
        postService.fetchPostData { (posts) in
            DispatchQueue.main.async {
                self.postList = posts
                for index in (0...posts.count-1) {
                    if index < 20 {
                        self.postList[index].isViewed = false
                    }else{
                        self.postList[index].isViewed = true
                    }
                }
                
                self.viewModel.posts = self.postList
                
            }
        }
    }
    
    func onDetailViewAppear(userID: Int){
        userService.fetchUsersData { (users) in
            DispatchQueue.main.async {
                self.userList = users
                for user in users {
                    if user.id == userID {
                        self.userSelected = user
                        self.viewModel.userSelected = user
                    }
                }
            }
        }
    }
    
    func loadComments(postID: Int){
        commentService.fetchComments(postID: postID) { (comments) in
            DispatchQueue.main.async {
                self.viewModel.comments = comments
            }
        }
    }
    
    func addToFavorite(postID:Int){
        for index in (0...postList.count-1) {
            if postList[index].id == postID {
                postList[index].isFavorite = !postList[index].isFavorite
                updateData(postsUpdated: postList)
            }
        }
    }
    
    func addToViewed(postID:Int){
        for index in (0...postList.count-1) {
            if postList[index].id == postID {
                postList[index].isViewed = true
                updateData(postsUpdated: postList)
            }
        }
    }
    
    func updateData(postsUpdated: [Post]){
        self.viewModel.posts = postsUpdated
    }
    
}
