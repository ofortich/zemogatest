//
//  Comment.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 12/12/21.
//

import Foundation

struct Comment: Codable, Identifiable {
    let postID, id: Int
    let name, email, body: String

    enum CodingKeys: String, CodingKey {
        case postID = "postId"
        case id, name, email, body
    }
}
