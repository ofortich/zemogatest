//
//  Post.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 11/12/21.
//

import Foundation

struct Post: Codable, Identifiable {
    var userID, id: Int
    var title, body: String
    var isViewed: Bool = false
    var isFavorite: Bool = false
    
    init(){
        self.userID = 0
        self.id = 0
        self.title = ""
        self.body = ""
    }

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}

/*
 ScrollView(.vertical) {
     LazyVStack(alignment: .leading, spacing: 20) {
         if !(self.viewModel.posts?.count == 0){
             if self.viewModel.posts != nil {
                 ForEach(0..<self.presenter.postList.count, id: \.self) { index in
                     
                     if favsSelected {
                         if self.presenter.postList[index].isFavorite {
                             NavigationLink(
                                 destination: PostDetailView(),
                                 label: {
                                     PostCellView(selected: self.presenter.postList[index].isViewed,
                                                  title: self.presenter.postList[index].title,
                                                  isFavorite: favsSelected)
//                                                        .onTapGesture {
//                                                            self.presenter.postList[index].isViewed = true
//                                                            self.presenter.updateData(postsUpdated: self.presenter.postList)
//                                                        }
                                 })
                             Divider()
                         }
                     } else {
                         NavigationLink(
                             destination: PostDetailView(),
                             label: {
                                 PostCellView(selected: self.presenter.postList[index].isViewed,
                                              title: self.presenter.postList[index].title,
                                              isFavorite: favsSelected)
//                                                    .onTapGesture {
//                                                        self.presenter.postList[index].isViewed = true
//                                                        self.presenter.updateData(postsUpdated: self.presenter.postList)
//                                                    }
                             })
                         Divider()
                     }
                     
                 }
             }
         }
     }.padding()
 }.onAppear{
     if presenter.shouldReload {
         presenter.onViewAppear()
         presenter.shouldReload = false
     }
 }
 */
