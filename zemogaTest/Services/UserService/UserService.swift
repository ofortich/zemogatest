//
//  UserService.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 12/12/21.
//

import Foundation

class UserService {
    
    func fetchUsersData(onCompletion: @escaping ([User]) -> Void) {
        
        let url = URL(string: "https://jsonplaceholder.typicode.com/users")!
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data else { return }
            
            do {
                let userData = try JSONDecoder().decode([User].self, from: data)
                onCompletion(userData)
            }
            catch {
                let error = error
                print(error.localizedDescription)
            }
            
        }.resume()
        
    }
    
}
