//
//  CommentService.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 12/12/21.
//

import Foundation

class CommentService{
    
    func fetchComments(postID:Int, onCompletion: @escaping([Comment]) -> Void){
        let url = URL(string: "https://jsonplaceholder.typicode.com/comments?postId=\(postID)")!
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data else { return }
            
            do {
                let commentData = try JSONDecoder().decode([Comment].self, from: data)
                onCompletion(commentData)
            }
            catch {
                let error = error
                print(error.localizedDescription)
            }
            
        }.resume()
    }
    
}
