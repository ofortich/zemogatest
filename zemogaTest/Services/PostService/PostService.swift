//
//  PostService.swift
//  zemogaTest
//
//  Created by Orlando Luis Fortich Millan on 12/12/21.
//

import Foundation


class PostService {
    
    func fetchPostData(onCompletion: @escaping ([Post]) -> Void) {
        
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts")!
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data else { return }
            
            do {
                let postData = try JSONDecoder().decode([Post].self, from: data)
                onCompletion(postData)
            }
            catch {
                let error = error
                print(error.localizedDescription)
            }
            
        }.resume()
        
    }
    
}
